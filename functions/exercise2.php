<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Functions Exercise 2</title>
</head>
<body>
<?php
/*Create a variable ‘year’ and create a function ‘isLeapYear’ that checks if the year is a leap 
year. If it is a leap year, return a Boolean value. In the exercises of the control structures, 
we discussed what a leap year is.

*/

$year = 2004;

function isLeapYear ($year) {
    if ($year % 400 == 0 || $year % 4 == 0) {
        return true;
    } else {
        return false;
    }
}

if (isLeapYear($year)) {
    echo $year . " is a leap year";
} else {
    echo $year . " is not a leap year";
}
?>
</body>
</html>