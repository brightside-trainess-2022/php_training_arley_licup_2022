<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Functions Exercise 1</title>
</head>
<body>
<?php
/*Create two variables ‘price’ and ‘vat’, and create a function called ‘calculateVat’ that takes 
two parameters, and returns a variable ‘calculatedPrice’. Print out the price, vat and total 
price.
*/

$price = 250;
$vat = 0.12; 

function calculateVat ($price, $vat) {
    $calculatedPrice = ($price * $vat) + $price;
    return $calculatedPrice;
}

echo "Price: ". $price . "<br>";
echo "VAT: ". $vat . "<br>";
echo "Total Price: " . calculateVAT($price, $vat) . "<br>";

?>
</body>
</html>