<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Functions Exercise 3</title>
</head>
<body>
<?php
/*Create two variables ‘num1’ and ‘num2’ and create four functions ‘addNumbers’, 
‘subtractNumbers’, ‘multiplyNumbers’ and ‘divideNumbers’. The function accepts two 
parameters, and return the addition, subtraction, multiplicity and division of the two 
numbers.
*/

$num1 = 5;
$num2 = 2;

function addNumbers ($num1, $num2) {
    return $num1 + $num2;
}

function subtractNumbers ($num1, $num2) {
    return $num1 - $num2;
}

function multiplyNumbers ($num1, $num2) {
    return $num1 * $num2;
}

function divideNumbers ($num1, $num2) {
    return $num1 / $num2;
}

echo "Addition of " . $num1 . " and " . $num2 . " is " . addNumbers($num1, $num2) . "<br>";
echo "Subtraction of " . $num1 . " and " . $num2 . " is " . subtractNumbers($num1, $num2) . "<br>";
echo "Multiplication of " . $num1 . " and " . $num2 . " is " . multiplyNumbers($num1, $num2) . "<br>";
echo "Division of " . $num1 . " and " . $num2 . " is " . divideNumbers($num1, $num2) . "<br>";

?>
</body>
</html>