<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Functions Exercise 5</title>
</head>
<body>
<?php
/*Create a function ‘evenOrNot’ that checks if a given number ‘num1’ is even or odd. Echo 
the expected output.
*/

$num1 = 3;

function evenOrNot ($num1) {
    if ($num1 % 2 == 0) {
        echo $num1 . " is even number";
    } else {
        echo $num1 . " is odd number";
    }
}

evenOrNot($num1);

?>
</body>
</html>