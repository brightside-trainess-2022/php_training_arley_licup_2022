<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Functions Exercise 4</title>
</head>
<body>
<?php
/*Create a function ‘swapNumbers’ that takes two parameters ‘num1’ and ‘num2’, and 
inside the function, that swaps the numbers.
*/

$num1 = 38;
$num2 = 74;

echo "Before swapping num 1 = " . $num1 . " and num 2 = " . $num2 . "<br>" . "<br>";

function swapNumbers ($num1, $num2) {
    $temp = $num1;
    $num1 = $num2;
    $num2 = $temp;

    echo "After swapping num 1 = " . $num1 . " and num 2 = " . $num2;
}

swapNumbers($num1, $num2);

?>
</body>
</html>