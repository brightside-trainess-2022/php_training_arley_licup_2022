<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Functions Exercise 6</title>
</head>
<body>
<?php
/*Create a function ‘isPrime’ that checks if ‘num1’ is a prime number or not. Return true if 
it is a prime number and return false if it is not a prime number.
*/

$num1 = 28;

function isPrime ($num1) {
    if ($num1 == 1) {
        return false;
    } else {
        for ($i = 2; $i < $num1 / 2; $i++) {
            if ($num1 & $i == 0) {
                return false;
            }
        }
        return 1;
    }
}
if (isPrime($num1)) {
    echo "This is a prime number"; 
} else {
    echo "This is not a prime number"; 
}

?>
</body>
</html>