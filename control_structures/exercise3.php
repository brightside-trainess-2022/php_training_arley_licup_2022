<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Control Structures Exercise 3</title>
</head>
<body>
<?php
/*The American grading system works with the letter A, B, C, D and F whereas C or higher 
means that you’ve passed the subject. Create a variable that shows the amount of points a 
student scored, and based on that number, output the letter that a student got.
Also, show if the student passed the exam or not.
• A = 90-100 points;
• B = 80-89 points;
• C = 70-79 points;
• D = 60-69 points;
• F = Fewer than 60 points.
*/

$score = 99;


if ($score >= 90 && $score <= 100) {
    echo "You received an A! You passed the exam!";
} else if ($score >= 80 && $score <= 89) {
    echo "You received a B! You passed the exam!";
} else if ($score >= 70 && $score <= 79) {
    echo "You received a C! You passed the exam!";
} else if ($score >= 60 && $score <= 69) {
    echo "You received a D! You passed the exam!";
} else {
    echo "You received a F! You failed this course!";
}

?>
</body>
</html>