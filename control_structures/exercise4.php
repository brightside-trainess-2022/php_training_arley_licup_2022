<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Control Structures Exercise 4</title>
</head>
<body>
<?php
/*A car manufacturer is replacing its machines. The machines will only be replaced if one 
or more of the following conditions is met:
• The machine has more than 10,000 working hours;
• The machine older than 7 years;
• The machine has more than 25 failures per year.
*/

$machineHours = 9000;
$machineAge = 7;
$machineFailures = 27;


if ($machineHours >= 10000 || $machineAge > 7 || $machineFailures >= 25) {
    echo "Machine needs to be replaced!";
} else {
    echo "Machine is in perfect condition!";
}
?>
</body>
</html>