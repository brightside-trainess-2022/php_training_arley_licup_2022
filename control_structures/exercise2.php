<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Control Structures Exercise 2</title>
</head>
<body>
<?php
/*Create a variable with a value in-between number 1 till 4 (Reject any other value). Based 
on the value, print out “Spades”, “Hearts”, “Diamonds” and “Clubs”.*/

$x = 1;


switch ($x) {
    case 1:
        echo "Spades";
    break;
    case 2:
        echo "Hearts";
    break;    
    case 3:
        echo "Diamonds";
    break;    
    case 4:
        echo "Clubs";
    break;
    default:
        echo "Sorry the value is not acceptable!";        
}
?>
</body>
</html>