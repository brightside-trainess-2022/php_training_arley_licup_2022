<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Control Structures Exercise 7</title>
</head>
<body>
<?php
/*Write a program to get the Fibonacci series from 0 to 50.
*/

$num1 = 0;
$num2 = 1;
$ctr = 0;

while ($ctr < 12) {
    echo ' ' . $num1;
    $num3 = $num2 + $num1;
    $num1 = $num2;
    $num2 = $num3;
    $ctr++;
}
?>
</body>
</html>