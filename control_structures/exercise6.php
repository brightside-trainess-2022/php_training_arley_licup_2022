<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Control Structures Exercise 6</title>
</head>
<body>
<?php
/*Write a program that displays the multiplication table of a given integer. Create a variable 
of the beginning and ending of the loop.
*/

$x = 1;
$y = 30;

while ($x <= $y) {
   $multiplication = $x * $y;
   echo $x . " x " . $y . " = " . $multiplication . "<br>";
   $x++;
}
?>
</body>
</html>