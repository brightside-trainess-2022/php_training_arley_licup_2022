<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Control Structures Exercise 1</title>
</head>
<body>
<?php
/*Create two variables with two integer numbers. Print out which one is the highest and 
lowest value.*/

$x = 28;
$y = 11;

if ($x > $y) {
    $highest = $x;
    $lowest = $y;
    echo "Highest value is: " . $highest . "<br>";
    echo "Lowest value is: " . $lowest;
} else if ($x > $y) {
    $highest = $y;
    $lowest = $x;
    echo "Highest value is: " . $highest . "<br>";
    echo "Lowest value is: " . $lowest;
}
?>
</body>
</html>