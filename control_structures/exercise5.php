<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Control Structures Exercise 5</title>
</head>
<body>
<?php
/*Create a variable with a year. Create a program that shows if the year is a leap year. (A 
year is a leap year is you can divide is by 400 or by 4). 
*/

$year = 1999;


if ($year % 400 == 0 || $year % 4 == 0) {
    echo $year . " is a leap year";
} else {
    echo $year . " is not a leap year";
}
?>
</body>
</html>