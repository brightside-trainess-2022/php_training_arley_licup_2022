<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Assignment Operators</title>
</head>
<body>
<?php
// Assignment Operators
//  +=  --- Add and assign
//  -=  --- Subtract and assign
//  *=  --- Multiply and assign
//  /=  --- Divide and assign
//  .=  --- Concatenate and assign

$a = 5;
$b = 10;
$c = 15;
$d = 20;

echo "Add and assign" . "<br>";
echo $a . " += 5 " . " = "; 
echo $a += 5;
echo "<br>" . "<br>";

echo "Subtract and assign" . "<br>";
echo $b . " -= 10 " . " = "; 
echo $b -= 10;
echo "<br>" . "<br>";

echo "Multiply and assign" . "<br>";
echo $c . " *= 15 " . " = "; 
echo $c *= 15;
echo "<br>" . "<br>";

echo "Divide and assign" . "<br>";
echo $d . " /= 20 " . " = "; 
echo $d /= 20;
echo "<br>" . "<br>";

$str = "Hello";
$str .= " World!";

echo "<br>";
echo $str;
?>
</body>
</html>