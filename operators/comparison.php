<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Comparison Operators</title>
</head>
<body>
<?php
// Comparison Operators
// ==    --- Equal
// ====  --- Identical
// !=    --- Not equal
// !==   --- Not identical
// >     --- Greater than
// <     --- Less than 
// >=    --- Greater than or equal to 
// <=    --- Less than or equal to 
// <=>   ---  Spaceship

$x = 17;
$y = 9;

echo "EQUAL" . "<br>";
echo $x . " == " . $y . "<br>";
if ($x == $y) {
    echo "True!" . "<br>" . "<br>";
} else {
    echo "False!" . "<br>" . "<br>";
}

echo "IDENTICAL" . "<br>";
echo $x . " === " . $y . "<br>";
if ($x === $y) {
    echo "True!" . "<br>" . "<br>";
} else {
    echo "False!" . "<br>" . "<br>";
}

echo "NOT EQUAL" . "<br>";
echo $x . " != " . $y . "<br>";
if ($x != $y) {
    echo "True!" . "<br>" . "<br>";
} else {
    echo "False!" . "<br>" . "<br>";
}

echo "NOT IDENTICAL" . "<br>";
echo $x . " !== " . $y . "<br>";
if ($x != $y) {
    echo "True!" . "<br>" . "<br>";
} else {
    echo "False!" . "<br>" . "<br>";
}

echo "GREATER THAN" . "<br>";
echo $x . " > " . $y . "<br>";
if ($x > $y) {
    echo "True!" . "<br>" . "<br>";
} else {
    echo "False!" . "<br>" . "<br>";
}

echo "LESS THAN" . "<br>";
echo $x . " < " . $y . "<br>";
if ($x < $y) {
    echo "True!" . "<br>" . "<br>";
} else {
    echo "False!" . "<br>" . "<br>";
}

echo "GREATER THAN OR EQUAL TO" . "<br>";
echo $x . " >= " . $y . "<br>";
if ($x >= $y) {
    echo "True!" . "<br>" . "<br>";
} else {
    echo "False!" . "<br>" . "<br>";
}

echo "LESS THAN OR EQUAL TO" . "<br>";
echo $x . " <= " . $y . "<br>";
if ($x <= $y) {
    echo "True!" . "<br>" . "<br>";
} else {
    echo "False!" . "<br>" . "<br>";
}

echo "SPACESHIP" . "<br>";
echo $x . " <=> " . $y . "<br>";
echo ($x <=> $y) 
?>  
</body>
</html>