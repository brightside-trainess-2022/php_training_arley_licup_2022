<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Increment and Decrement Operators</title>
</head>
<body>
<?php
// ++$x   --- Pre-increment
// $x++   --- Post-increment
// --$x   --- Pre-decrement
// $x--   --- Post-decrement

$a = 36;
$b = 55;
$c = 98;
$d = 71;

echo "PRE-INCREMENT" . "<br>" . $a . "<br>";
echo ++$a . "<br>" . "<br>";


 
echo "POST-INCREMENT" . "<br>" . $b . "<br>";
echo $b++ . "<br>" . "<br>";


echo "PRE-DECREMENT" . "<br>" . $c . "<br>";
echo --$c . "<br>" . "<br>";


echo "POST-DECREMENT" . "<br>" . $d . "<br>";
echo $d-- . "<br>" . "<br>";



?>  
</body>
</html>