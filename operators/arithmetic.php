<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Arithmetic Operators</title>
</head>
<body>
<?php
// Arithmetic Operators
// + Addition 
// - Substraction
// * Multiplication
// / Division 
// % Modulo 

$x = 135;
$y = 52;

echo "Addition" . "<br>";
$add = $x + $y;

echo $x . " + " . $y . " = " . $add . "<br>" . "<br>";

echo "Substraction" . "<br>";
$sub = $x - $y;

echo $x . " - " . $y . " = " . $sub . "<br>" . "<br>";

echo "Multiplication" . "<br>";
$mul = $x * $y;

echo $x . " * " . $y . " = " . $mul . "<br>" . "<br>";

echo "Division" . "<br>";
$div = $x / $y;

echo $x . " / " . $y . " = " . $div . "<br>" . "<br>";

echo "Modulo" . "<br>";
$mod = $x % $y;

echo $x . " % " . $y . " = " . $mod . "<br>" . "<br>";
?>
</body>
</html>