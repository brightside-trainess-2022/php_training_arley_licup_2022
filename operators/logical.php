<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Logical Operators</title>
</head>
<body>
<?php
//Logical operators
// And  --- Both X and Y are true
// &&   --- Both X and Y are true
// Or   --- Either X or Y are true
// ||   --- Either X or Y are true
// Xor  --- Either X or Y are true, not both
// !    --- True if X is not true
// 1 = True - 0 = False

$x = 5;
$y = 10;

echo "AND" . "<br>";
echo $x . " == " . $y . " && " . "1 == 1" . "<br>";
if ($x == $y && 1 == 1) {
    echo "True!" . "<br>" . "<br>";
} else {
    echo "False!" . "<br>" . "<br>";
}


echo "OR" . "<br>";
echo $x . " == " . $y . " || " . "1 == 1" . "<br>";
if ($x == $y && 1 == 1) {
    echo "True!" . "<br>" . "<br>";
} else {
    echo "False!" . "<br>" . "<br>";
}

echo "XOR" . "<br>";
echo $x . " == " . $y . " xor " . "1 == 1" . "<br>";
if ($x == $y xor 1 == 1) {
    echo "True!" . "<br>" . "<br>";
} else {
    echo "False!" . "<br>" . "<br>";
}


echo "NOT" . "<br>";
echo $x . " == " . $y . " ! " . "1 == 1" . "<br>";
if (!$x == $y && 1 == 1) {
    echo "True!" . "<br>" . "<br>";
} else {
    echo "False!" . "<br>" . "<br>";
}


?>
</body>
</html>
