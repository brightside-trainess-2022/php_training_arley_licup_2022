<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Average</title>
</head>
<body>
<?php
/*Create three variables ‘x’, ‘y’, ‘z’ and calculate the average ‘average’ of the numbers and
print it out on the screen. Be aware that the average is a decimal number, so use a
function number_function in PHP.*/

$x = 5;
$y = 16;
$z = 11;

$average = ($x + $y + $z) / 3;

echo number_format($average, 2, ".", ' ');
?>
</body>
</html>
