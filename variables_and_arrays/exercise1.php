<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Variables</title>
</head>
<body>
<?php
/*Create two variables (‘x’, ‘y’) add them, and multiply it by 5. 
Assign the output to a new variable ‘z’.*/

$x = 12;
$y = 10;

$z = ($x + $y) * 5;

echo "The total is " . $z;
?>
</body>
</html>
