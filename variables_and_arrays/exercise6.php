<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Expenses</title>
</head>
<body>
<?php
/*Create an array ‘expenses’ with your biggest expenses of the month. Loop through the
array and add the expenses in a integer ‘totalAmount’. Finally, display the total expenses
that you had and the amount of values ‘amountOfExpenses’ you had stored inside of your
array.*/

$expenses = array(500, 1200, 6000);

$totalAmount = 0;
$amountOfExpenses = 0;

foreach($expenses as $expense) {
    $totalAmount = $expense + $totalAmount;
    $amountOfExpenses = $amountOfExpenses + 1; 
}

echo "My " . $amountOfExpenses . " biggest expenses were " . $totalAmount; 

?>
</body>
</html>
