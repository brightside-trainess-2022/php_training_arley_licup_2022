<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Countries and Capitals</title>
</head>
<body>
<?php
/*Create an array ‘countries’ that displays 5 countries and the capital names.*/

$countries = array("Philippines" => "Manila", 
                    "Norway" => "Oslo", 
                    "Israel" => "Jerusalem",
                    "Japan" => "Tokyo",
                    "South Korea" => "Seoul");

foreach($countries as $key => $value) {
    echo "The capital of " . $key . " is " . $value;
    echo "<br>";
}
?>
</body>
</html>
