<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Weather Conditions</title>
</head>
<body>
<?php
/*Create an array ‘weather’ of weather conditions with the following values: rain, sunshine,
clouds, hail, sleet, snow, wind. Using the array variable for all the weather conditions,
echo the following statement to the browser:

We've seen all kinds of weather this month. At the beginning of the month, we had
snow and wind. Then came sunshine with a few clouds and some rain. At least we
didn't get any hail or sleet.*/

$weather = array("Rain", "Sunshine", "Clouds", "Hail", "Sleet", "Snow", "Wind");

echo "We've seen all kinds of weather this month. 
        At the beginning of the month, we had " . $weather[5] . " and " . $weather[6] . 
        ". Then came sunshine with a few " . $weather[2] . " and some " . $weather[0] . 
        ". At least we didn't get any " . $weather[3] . " or " . $weather[4] . ".";
?>
</body>
</html>
