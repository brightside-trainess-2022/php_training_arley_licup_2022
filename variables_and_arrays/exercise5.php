<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Centimeters to Inches</title>
</head>
<body>
<?php
/*Create an integer ‘cmToInch’ that converts a number of centimeters ‘cm’ to inches ‘inch’.
(tip: 1 centimeter is equivalent to 0.39 inch).*/

$cm = 50;

$cmToInch = $cm * 0.39;

echo $cm . " centimeter is " . $cmToInch . " inch";
?>
</body>
</html>
