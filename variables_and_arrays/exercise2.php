<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Value Added Tax</title>
</head>
<body>
<?php
/*Create two variables ‘price’ and ‘vat’. Create a new variable ‘totalPrice’ and calculate the vat on the price 
and print out the price, vat and total price.*/

$price = 300;
$vat = 0.25;

$totalPrice = ($price * $vat) + $price;

echo "Price: " . $price . "<br>";
echo "Vat: " . $vat . "<br>";
echo "Total Price: " . $totalPrice;
?>
</body>
</html>

